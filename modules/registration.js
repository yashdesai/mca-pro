const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    userType: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    address: {
        type: String,
        required: true,
    },
    state: {
        type: String,
        required: true,
    },
    city: {
        type: String,
        required: true,
    },
    zipcode: {
        type: Number,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
});

const Register = new mongoose.model("Register", userSchema);

module.exports = Register;
