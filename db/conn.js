const mongoose = require("mongoose");

mongoose
    .connect("mongodb://localhost/TestDb")
    .then(() => console.log(`Connection Success`))
    .catch((e) => console.log(e));
